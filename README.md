# Delivery Driver

## Description

A simple delivery game created while learning the C# Unity Game Developer 2D course by gamedev.tv. The intended purpose of this game was to teach me how to use rigid bodies, collisions, assets, and other basic Unity functions. The player moves around a pink car that can pick up packages indicated by a colored square. The picking up of a package is indicated by a color change of the car. There are also customers, that are red circles, that the packages can be delivered too. Speed boosts, represented by the orange circles, are also used to make the car go faster. There are also a multitudes of rocks and houses that can be bumped into that reduces the speed of the car to a set value.

### Dependencies

- Unity v2020.3.22f1

### Executing program

Create a new project on unity. Add all the following files within the base folder of the unity project. Go to build settings using ctrl + shift + b to build the project to the desired settings in a new folder to generate the executable for the game.

